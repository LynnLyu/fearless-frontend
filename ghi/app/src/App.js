// import Nav from './Nav';
// // import AttendeesList from './AttendeesList';
// import LocationForm from './LocationForm';
// import ConferenceForm from './ConferenceForm';
// import ConferenceList from './ConferenceList';
// import AttendConferenceForm from './AttendConferenceForm';
// import { BrowserRouter as Router } from "react-router-dom";
// import { Switch, Route } from "react-router-dom";

// function App(props) {
//   if (props.attendees === undefined) {
//     return null;
//   }
//   return (
//     <BrowserRouter>
//       <Nav />
//       {/* <div className="container"> */}
//         <Router>
//           <Switch>
//             <Route path="locations"></Route>
//             <Route path="new" element={<LocationForm />} />
//           </Switch>
//         </Router>
//       {/* </div> */}
//     </BrowserRouter>
//   );
// }

// export default App;

import logo from './logo.svg';
import './App.css';
import Nav from './Nav';
import AttendeesList from './AttendeesList';
import LocationForm from './LocationForm';
import ConferenceForm from './ConferenceForm';
import AttendConferenceForm from './AttendConferenceForm';
import PresentationForm from './PresentationForm';
import { BrowserRouter,Routes,Route } from "react-router-dom";
import MainPage from './MainPage';

function App(props) {
  if (props.attendees === undefined) {
    return null;
  }
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="locations">
            <Route path="new" element={<LocationForm />} />
          </Route>
          <Route path="conferences">
            <Route path="new" element={<ConferenceForm />} />
          </Route>
          <Route path="presentations">
            <Route path="new" element={<PresentationForm />} />
          </Route>

          <Route path="attendees">
            <Route  path="new" element={<AttendConferenceForm />} />
            <Route  path="" element={<AttendeesList attendees={props.attendees} />} />

          </Route>

          <Route index element={<MainPage />} />






        </Routes>
        {/* <AttendeesList attendees={props.attendees} /> */}
        {/* <ConferenceForm /> */}
        {/* <AttendeeForm /> */}
      </div>
    </BrowserRouter>
  );
}

export default App;