import React from 'react'

function ConferenceList(props) {
    return (
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Href</th>
                    <th>Conference</th>
                </tr>
            </thead>
            <tbody>
                {props.conferences.map(conference => {
                    return (
                    <tr key={conference.href}>
                        <td>{ conference.name }</td>
                        <td>{ conference.description }</td>

                    </tr>
                    );
                })}
            </tbody>
        </table>
    );
}

export default ConferenceList;