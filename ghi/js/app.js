window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/conferences/';

    try {
      const response = await fetch(url);

      if (!response.ok) {
        // Figure out what to do when the response is bad
      } else {
        const data = await response.json();

        for (let conference of data.conferences) {
          const detailUrl = `http://localhost:8000${conference.href}`;
          const detailResponse = await fetch(detailUrl);
          if (detailResponse.ok) {
            const details = await detailResponse.json();
            const title = details.conference.name;
            const description = details.conference.description;
            const pictureUrl = details.conference.location.picture_url;
            const startDate = details.conference.starts;
            console.log("startDate")

            const start = new Date(startDate).toLocaleDateString();
            console.log(start)
            const endDate = details.conference.ends;

            const end = new Date(endDate).toLocaleDateString();
            console.log(end)

            const locations_name = details.conference.location.name;


            const html = createCard(title, description, pictureUrl, start, end, locations_name);
            const column = document.querySelector('.row');
            column.innerHTML += html;          }
        }

      }
    } catch (e) {
      // Figure out what to do if an error is raised
        alert("There is an error!")
    }
    function createCard(name, description, pictureUrl, start, end, locations_name) {
        return `
        <div class="col-3">
          <div class="card shadow-lg">
            <img src="${pictureUrl}" class="card-img-top">
            <div class="card-body">

              <h5 class="card-title">${name}</h5>
              <p class="card-text">${locations_name}</p>
              <p class="card-text">${description}</p>
              <p class="card-text">${start} - ${end}</p>


            </div>
          </div>
        </div>
        `;
    }

  });

//     const url = 'http://localhost:8000/api/conferences/';

//     try {
//       const response = await fetch(url);

//       if (!response.ok) {
//         // Figure out what to do when the response is bad
//       } else {
//         const data = await response.json();
//         const conference = data.conferences[0];
//         const nameTag = document.querySelector('.card-title');
//         nameTag.innerHTML = conference.name;

//         const detailUrl = `http://localhost:8000${conference.href}`;
//         const detailResponse = await fetch(detailUrl);
//         if (detailResponse.ok) {
//             const details = await detailResponse.json();

//             const des = details.conference.description;


//             const descriptionTag = document.querySelector('.card-text');

//             descriptionTag.innerHTML = des;

//             const imageTag = document.querySelector(".card-img-top")
//             imageTag.src = details.conference.location.picture_url;

//         }


//       }
    //   function createCard(name, description, pictureUrl) {
    //     return `
    //       <div class="card">
    //         <img src="${pictureUrl}" class="card-img-top">
    //         <div class="card-body">
    //           <h5 class="card-title">${name}</h5>
    //           <p class="card-text">${description}</p>
    //         </div>
    //       </div>
    //     `;
    //   }
//     } catch (e) {
//       // Figure out what to do if an error is raised
//         // console.error("It is a error");
//     }

//   });
